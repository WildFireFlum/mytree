//
// Created by Ynon on 13/07/2019.
//
#include <set>
#include <gtest/gtest.h>
#include <memory>
#include "../Libs/mingw-threading/thread.h"
#include "../Allocator/MockAllocator.h"
#include "../LockFreeBinTree.h"

using flum::LockFreeBinTree;

TEST(TESTBintree, TestSequential) {
    MockAllocator allocator;
    std::srand(0xdeadbeef);
    std::set<unsigned int> numbers_to_add;
    const uint32_t numbers_to_add_count = 1 << 10;

    for (uint32_t i = 0; i < numbers_to_add_count; i++) {
        numbers_to_add.insert(std::rand());
    }

    LockFreeBinTree<unsigned int> tree(allocator, *numbers_to_add.begin());

    for (auto num : numbers_to_add) {
        tree.insert(num);
    }

    auto actual_results = tree.getAllValues();
    EXPECT_EQ(actual_results, numbers_to_add);
}

TEST(TESTBintree, TestMaxDepth) {
    MockAllocator allocator;
    std::vector<unsigned int> long_chain = {4, 5, 6, 7, 8};
    std::vector<unsigned int> short_chain = {1, 2};

    LockFreeBinTree<unsigned int> tree(allocator, *long_chain.begin());

    for (auto num : long_chain) {
        tree.insert(num);
    }

    for (auto num : short_chain) {
        tree.insert(num);
    }

    EXPECT_EQ(tree.getMaxDepth(), long_chain.size());
}

TEST(TESTBintree, TestRebalance) {
    MockAllocator allocator;
    constexpr uint32_t chain_length = 1u << 10u;
    std::vector<uint32_t> long_chain(chain_length);
    for (uint32_t i = 0; i < chain_length; i++) {
        long_chain.push_back(i);
    }

    LockFreeBinTree<uint32_t> tree(allocator, *long_chain.begin());

    for (auto num : long_chain) {
        tree.insert(num);
    }

    tree.rebalance();
    EXPECT_EQ(tree.getAllValues().size(), chain_length);
    EXPECT_LT(tree.getMaxDepth(), chain_length);
}

TEST(TESTBintree, TestParallelInsertions) {
    MockAllocator allocator;
    std::srand(0x18891889);
    constexpr uint32_t root_value = 20000u;
    constexpr uint32_t thread_count = 4;
    auto tree_ptr = std::make_unique<LockFreeBinTree<uint32_t>>(allocator, root_value);
    std::vector<std::thread> threads;

    const uint32_t numbers_to_add_per_thread = 1 << 4;
    auto numbers_to_add_ptr = std::make_unique<std::set<uint32_t>[]>(thread_count);

    for (uint32_t i = 0u; i < thread_count; i++) {
        for (uint32_t j = 0u; j < numbers_to_add_per_thread; j++) {
            numbers_to_add_ptr[i].insert(std::rand());
        }
    }
    std::cout << "Finished generating numbers" << std::endl;

    for (uint32_t i = 0u; i < thread_count; i++) {
        std::cout << "Starting inserting thread " << i << std::endl;
        threads.emplace_back(
            [numbers_to_add = numbers_to_add_ptr[i], tree = tree_ptr.get()]() {
              for (auto& num : numbers_to_add) {
                  tree->insert(num);
              }
            });
    }

    for (auto& thread : threads) {
        thread.join();
    }

    std::cout << "Threads joined." << std::endl;

    std::set<uint32_t> all_numbers_to_add;
    for (uint32_t i = 0u; i < thread_count; i++) {
        all_numbers_to_add.merge(numbers_to_add_ptr[i]);
    }
    all_numbers_to_add.insert(root_value);

    auto actual_results = tree_ptr->getAllValues();
    std::cout << "Expected size: " << all_numbers_to_add.size() << std::endl;

    for (auto result : all_numbers_to_add) {
        if (actual_results.find(result) == actual_results.end()) {
            std::cout << "Value " << result << " not inserted" << std::endl;
        }
    }

    EXPECT_EQ(actual_results.size(), all_numbers_to_add.size());
    EXPECT_EQ(actual_results, all_numbers_to_add);
}
