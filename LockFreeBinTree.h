//
// Created by Ynon on 13/07/2019.
//

#pragma once

#include <atomic>
#include <tuple>
#include <deque>
#include <set>
#include "Allocator/Allocator.h"
#include "LockFreeBinTreeNode.h"
#include "Trunk.h"

namespace flum {

template<typename T>
class LockFreeBinTree {

  using node_t = LockFreeBinTreeNode<T>;

public:
  LockFreeBinTree(Allocator& allocator, T firstVal);

  bool insert(const T& newVal);

  bool exists(const T& searchedVal) const;

  std::set<T> getAllValues() const;

  void prettyPrint() const;

  uint32_t getMaxDepth() const;

  void rebalance();

private:

  node_t* allocateNode();

  std::tuple<node_t*, bool> find(const T& searchedVal) const;

  bool isLeaf(const node_t* node) const;

/**
 * Helper function to print branches of the binary tree
 * @param p
 */
  void showTrunks(Trunk* p) const;

/**
 * Recursive function to print binary tree
 * It uses inorder traversal
 * @param root
 * @param prev
 * @param isLeft
 */
  void printTree(node_t* root, Trunk* prev, bool isLeft) const;

  Allocator& m_allocator;
  std::atomic<node_t*> m_root;
};

}

#include "LockFreeBinTree.inl"

