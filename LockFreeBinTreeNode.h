//
// Created by Ynon on 27/07/2019.
//

#pragma once
#include <atomic>

namespace flum {

template<typename T>
struct LockFreeBinTreeNode {
  LockFreeBinTreeNode(const T& val, LockFreeBinTreeNode* left, LockFreeBinTreeNode* right) :
      m_val(val), m_right(right), m_parent(nullptr), m_left(left), m_weight(0) {}

  bool hasLeft() const {
      return m_left.load() != nullptr;
  }

  bool hasRight() const {
      return m_right.load() != nullptr;
  }

  T m_val;
  std::atomic_int32_t m_weight;
  std::atomic<LockFreeBinTreeNode*> m_parent;
  std::atomic<LockFreeBinTreeNode*> m_left;
  std::atomic<LockFreeBinTreeNode*> m_right;
};

}
