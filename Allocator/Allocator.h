#pragma once
class Allocator {
public:

  Allocator() = default;

  virtual void* allocate(unsigned int numOfBytes, unsigned int listIndex) = 0;

  virtual void deallocate(void* ptr, unsigned int listIndex) = 0;

  virtual void reclaim(void* ptr, unsigned int listIndex) = 0;
};
