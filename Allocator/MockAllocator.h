#pragma once

#include <iostream>
#include <atomic>
#include "Allocator.h"

// 10 MBS
#define MOCK_ALLOC_SIZE (1024 * 1024 * 10)

/**
 * A fixed size non-releasable synchronized buffer
 */
template<uint32_t N = 24>
class MockAllocator : public Allocator {
public:

  MockAllocator() : m_buf{new unsigned char[MOCK_ALLOC_SIZE]}, m_offset(0), m_allocations_count{0} {}

  void* allocate(unsigned int numOfBytes, unsigned int listIndex) {

      unsigned int old_offset = m_offset.fetch_add(numOfBytes);
      if (listIndex < N) {
          m_allocations_count[listIndex].fetch_add(1u);
          //std::cout << getThreadId() << ") counters["<<listIndex <<"] = " << m_allocations_count[listIndex] << " --- offset " << m_offset <<  std::endl;
      } else {
          std::cout << "Error, allocating to an unidentified list" << std::endl;
          throw;
      }
      return reinterpret_cast<void*>(m_buf.get() + old_offset);
  }

  void deallocate(void* ptr, unsigned int listIndex) {
      // Do not release memory in mock
  }

  void reclaim(void* ptr, unsigned int listIndex) {
      // Do not release memory in mock
  }

  unsigned int getNumOfAllocs(unsigned int listIndex) {
      return m_allocations_count[listIndex];
  }

  void clear() {
      memset(this, 0, sizeof(*this));
  }

  std::unique_ptr<unsigned char> m_buf;
  std::atomic<unsigned int> m_offset;
  std::atomic<unsigned int> m_allocations_count[N];
};
