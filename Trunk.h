//
// Created by Ynon on 27/07/2019.
//
#pragma once
#include <string>

namespace flum {

struct Trunk {
  Trunk(Trunk* prev, std::string str) : prev(prev), str(str) {}

  Trunk* prev;
  std::string str;

};

}
