//
// Created by Ynon on 27/07/2019.
//

#pragma once

#include "LockFreeBinTree.h"
#include <deque>

namespace flum {

template<typename T>
LockFreeBinTree<T>::LockFreeBinTree(Allocator& allocator, T firstVal) :
    m_allocator(allocator),
    m_root(nullptr) {
    m_root = allocateNode();
    m_root.load()->m_val = firstVal;
}

template<typename T>
bool LockFreeBinTree<T>::insert(const T& newVal) {
    while (true) {
        auto[newNodeParent, hasValue] = find(newVal);
        if (hasValue) {
            //std::cout << "Thread " << std::this_thread::get_id() << " Has value " << newVal << std::endl;
            return false;
        }

        bool isRightSon = newNodeParent->m_val < newVal;
        auto& ptrToChange = isRightSon ? newNodeParent->m_right : newNodeParent->m_left;

        auto* newNodePtr = allocateNode();
        newNodePtr->m_val = newVal;
        newNodePtr->m_parent = newNodeParent;

        node_t* nullNodePtr = nullptr;
        if (ptrToChange.compare_exchange_weak(nullNodePtr, newNodePtr)) {
            auto* currentNode = newNodeParent;
            while (currentNode != nullptr) {
                currentNode->m_weight.fetch_add(1u);
                currentNode = currentNode->m_parent;
            }
            return true;
        }
        std::cout << "Thread " << std::this_thread::get_id() << " didn't insert " << newVal << std::endl;
        m_allocator.deallocate(reinterpret_cast<void*>(newNodePtr), 0);
    }
}

template<typename T>
bool LockFreeBinTree<T>::exists(const T& searchedVal) const {
    return find(searchedVal)[1];
}

template<typename T>
std::set<T> LockFreeBinTree<T>::getAllValues() const {
    std::set<T> values;
    std::deque<node_t*> nodes_to_process;
    nodes_to_process.push_back(m_root.load());
    while (!nodes_to_process.empty()) {
        auto currentNode = nodes_to_process.front();
        values.insert(currentNode->m_val);
        if (currentNode->hasLeft()) {
            nodes_to_process.push_back(currentNode->m_left);
        }
        if (currentNode->hasRight()) {
            nodes_to_process.push_back(currentNode->m_right);
        }
        nodes_to_process.pop_front();
    }
    return values;
}

template<typename T>
void LockFreeBinTree<T>::prettyPrint() const {
    printTree(m_root.load(), nullptr, false);
}

template<typename T>
typename LockFreeBinTree<T>::node_t* LockFreeBinTree<T>::allocateNode() {
    auto* newNode = reinterpret_cast<node_t*>(m_allocator.allocate(sizeof(node_t), 0u));
    newNode->m_left = nullptr;
    newNode->m_right = nullptr;
    newNode->m_parent = nullptr;
    newNode->m_weight = 0;
    return newNode;
}

template<typename T>
std::tuple<typename LockFreeBinTree<T>::node_t*, bool> LockFreeBinTree<T>::find(const T& searchedVal) const {
    node_t* currentNode = m_root;
    node_t* newNodeVal;
    bool has_node = false;

    if (currentNode == nullptr) {
        return std::make_tuple(currentNode, has_node);
    }

    // Invariant: currentNode != nullptr
    while (true) {
        if (isLeaf(currentNode)) {
            break;
        }

        if (searchedVal > currentNode->m_val) {
            newNodeVal = currentNode->m_right.load();
        } else {
            newNodeVal = currentNode->m_left.load();
        }

        if (newNodeVal == nullptr) {
            break;
        }

        currentNode = newNodeVal;
    }

    if (searchedVal == currentNode->m_val) {
        has_node = true;
    }
    return std::make_tuple(currentNode, has_node);
}

template<typename T>
bool LockFreeBinTree<T>::isLeaf(const node_t* node) const {
    return !(node->hasLeft() || node->hasRight());
}

template<typename T>
void LockFreeBinTree<T>::showTrunks(Trunk* p) const {
    if (p == nullptr) {
        return;
    }

    showTrunks(p->prev);

    std::cout << p->str;
}

template<typename T>
void LockFreeBinTree<T>::printTree(node_t* root, Trunk* prev, bool isLeft) const {
    if (root == nullptr) {
        return;
    }

    std::string prev_str = "    ";
    Trunk* trunk = new Trunk(prev, prev_str);

    printTree(root->m_left, trunk, true);

    if (!prev) {
        trunk->str = "---";
    } else if (isLeft) {
        trunk->str = ".---";
        prev_str = "   |";
    } else {
        trunk->str = "`---";
        prev->str = prev_str;
    }

    showTrunks(trunk);
    std::cout << root->m_weight << std::endl;

    if (prev) {
        prev->str = prev_str;
    }
    trunk->str = "   |";

    printTree(root->m_right, trunk, false);
}
template<typename T>
uint32_t LockFreeBinTree<T>::getMaxDepth() const {
    std::deque<node_t*> current_level_queue;
    std::deque<node_t*> next_level_queue;

    uint32_t max_depth = 1u;
    current_level_queue.push_back(m_root.load());

    while (!current_level_queue.empty()) {
        for (auto& node_ptr : current_level_queue) {
            if (node_ptr->hasRight()) {
                next_level_queue.push_back(node_ptr->m_right.load());
            }
            if (node_ptr->hasLeft()) {
                next_level_queue.push_back(node_ptr->m_left.load());
            }
        }

        if (!next_level_queue.empty()) {
            max_depth++;
        }

        current_level_queue = std::move(next_level_queue);
    }

    return max_depth;
}

template<typename T>
void LockFreeBinTree<T>::rebalance() {
    auto all_values_set = getAllValues();
    std::vector<T> all_values(all_values_set.begin(), all_values_set.end());
    std::sort(all_values.begin(), all_values.end());

    std::deque<std::tuple<uint32_t, uint32_t>> current_layer;

    current_layer.push_back(std::make_tuple(0, all_values.size()));
    LockFreeBinTree balanced_tree(m_allocator, all_values[all_values.size() / 2]);
    while (!current_layer.empty()) {
        std::deque<std::tuple<uint32_t, uint32_t>> next_layer;
        for (auto[left_index, right_index] : current_layer) {
            uint32_t mid_point = (right_index + left_index) / 2;
            balanced_tree.insert(all_values[mid_point]);
            if (mid_point == right_index || mid_point == left_index) {
                break;
            }
            next_layer.push_back(std::make_tuple(left_index, mid_point));
            next_layer.push_back(std::make_tuple(mid_point, right_index));
        }
        current_layer = std::move(next_layer);
    }
    m_root = balanced_tree.m_root.load();
}

}

